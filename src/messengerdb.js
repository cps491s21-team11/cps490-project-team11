const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://jiangh8:jhw888@messengerdb.jfbrr.mongodb.net/messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}
const  getUserAge = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {

     console.log("age:  " + user.age);
     return user.age
     }
     return "";
}

const  getUserCollege = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {
     console.log("college:  " + user.college);
     return user.college
     }
     return "";
}
const  getUserGender = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {
     console.log("gender:  " + user.gender);
     return user.gender
     }
     return "";
}
const  checklogin = async (username,password)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username,password:password});
    if(user!=null && user.username==username){
        console.log("Debug>messengerdb.checklogin-> user found:\n" +
        JSON.stringify(user))
        return true 
    }
    return null
}
const queryUser = async () => {
    var users = getDb().collection("users");
    let list = await users.find({}).toArray();

    return list;
}

const addGroup = async (userlist) => {
    var group = getDb().collection("group");
    var users = getDb().collection("users");
    var newUser = {userlist: userlist};
    var result = await group.insertOne(newUser);
    /// is result is error
    let id = result.ops[0]._id;
    for (let user of userlist) {
        let res = await users.update({username: user}, {
            $set: {
                "groupid": id
            }
        })
    }
    return id;

}

const addUser = (username,password,age,college,gender,callback)=>{
    console.log("Debug>messengerdb.addUser:"+ username + "/" + password)
    var users = getDb().collection("users");
    users.findOne({username:username}).then(user=>{
      if(user && user.username === username) {
        console.log(`Debug>messengerdb.addUser: Username '${username}' exists!`);
        callback("UserExist");
        }
      else
      {
          var newUser = {"username": username, "password": password ,"age":age,"college":college,"gender":gender}
          users.insertOne(newUser,(err,result)=>{
              if(err){
                  console.log("Debug>messengerdb.addUser: error for adding '" +
                               username +"':\n", err);
                  callback("Error");
              }
              else
              {
                  console.log("Debug>messengerdb.addUser: a new user added: \n",
                               result.ops[0].username);
                  callback("Success")
              }

          })


      }

})
    //callback("a database handling message")
}

module.exports = {checklogin,addUser, queryUser, addGroup,getUserAge,getUserGender,getUserCollege};