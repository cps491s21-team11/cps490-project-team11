
CPS 490 - Capstone I, Semester Year

Instructor: Dr. Phu Phung

### Capstone I Project

## The Messenger Application-----Sprint 2

## Team members:

1.Hanwen Jiang,jiangh8@udayton.edu

2.Siqiang Fan,fans03@udayton.edu

3.Mingqing Luo,luom02@udayton.edu

# Project Management Information

Management board (private access): https://trello.com/b/tnMWWX1T/massenger

Source code repository (private access): https://bitbucket.org/cps490f20-team11/cps490-project-team11/src/Sprint-2/

latest commit: https://bitbucket.org/cps490f20-team11/cps490-project-team11/commits/4c7d652e70ec8d356c4442a72f016bbbf5244e4a

## Overview

This Sprint focus on implementing the user registration and user authentication, unregistered user cannot login. Registered user once login and they can create group chat and so forth.


## System Analysis


### User Requirements:

 User can register a new account
 
 User can use the registered account to login 
 
 User can send and receive once login
 
 User can log out
 
 User can create group chat and send, receive message in group
 

### System Requirement:

 System will send an alert when the user account does not exist, duplicate username, invalid password
 
 System will clear the data after user send the data
 
 System will send an alert when user enter the invalid format of username and password
 
  

### Use cases

#### general use case diagram

![use case diagram](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f75fb3abd9a9e884364cd60/b3bcd5993e0fc279a107e709b7a7e7f5/user_diagram.png)

#### Register an account

 User Story: As a user, i want to create an account by entering valid username and password
 
 Brief Use Case Description: users enter their email id and password, and the system will authenticate whether the input is valid, if it is valid, the system will create an new user account and add to the database.
 
 Fully Developed Use Case Description: 

 ![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f9885f70ed68b256550c3d8/695x588/67377f604792a80fa24632d2a4d998ef/us1.PNG.png)
 
 
 
#### Edit and Check friends' personal information
 
 User Stroy:As a user, I want to find  Who may know me in reality, and what kind of people are my friends.
 
 Brief Use Case Description:  user can check their friends information after added firends, so know what kind of people he added
 
 ![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f99073ea4d1d18b762d4b56/e57247362330d02a555494ed99bc9337/20201028134913.png)

 ![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990735be7e203b8f53abb4/48373420f3caa16a56fdb17446a61f98/20201028134745.png)
 
 

#### Login with valid username and password

 User Story: As a user, i want to login by entering my username and password which is previously created
 
 Brief Use Case Description: users enter their registered username and password, and the system will check whether the username exist or not and the password is valid or not, once successfully login in, the system will add the user to the chat.
 
 Fully Developed Use Case Description: 
 
 ![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f988857c5372b10ee89d21b/693x574/5fca9ce07f06b7f973afd872e67b45aa/us2.PNG.png)
 

#### Create group chat and send, receive message in group

 User Story: As a user, I want to Create group chat and send, receive message in group
 
 Brief Use Case Description: A user who has successfully registered can add other registered users to the group he or she has created. Members of a group can send information to each other.
 
 Fully Developed Use Case Description: 
 
![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f993c411f1d6f33702eb4af/583cc82b246d9181043defb51711dc7c/1.PNG)


#### Log out
 
 User Stroy: As a user, i want to log out.
 
 Brief Use Case Description: A user once successfully log out, the system will disconnect the user and will send an notification to other people
 
 Fully Developed Use Case Description: 
 
 ![Fully Use Case](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f988f554b5cc7504f3798f6/00cbebd78f175a1a2b32e860beeb6de5/us3.PNG)


## System Design


### System Architecture

#### Technology: 
Node.js

MongoDB

#### Application Architecture:

A web application with WebSocket, and the client/server architecture includes three layers which are view, domain, and data layer.

###### View layer

the user interface

###### Domain layer

logic layer, program logic to implement the functions

###### Data layer

functions that access the data

![System Architecture](https://trello-attachments.s3.amazonaws.com/5f75fb56cb2eec29dc109003/456x201/7705e38eb722259595dac664b8edeca9/system_arch.drawio_%281%29.png)

![System Architecture](https://trello-attachments.s3.amazonaws.com/5f75fb56cb2eec29dc109003/688x105/2da8f4b85509bb341255740b931e7892/system_arch.png)

### Use-Case Sequence Diagram

#### upload user information

![sequence diagram](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9ae112dfd57f0eb0661eb0/4364c88e93ca6ed600c8b5fae755bc79/CPS490_-01.png)

#### check firends information

![sequence diagram](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9ae112dfd57f0eb0661eb0/833df605e9b7404b524ed37e0cc4ff97/CPS490_-02.png)

##### Group chat

![sequence diagram](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f996cbe03241e1cbce77742/45f7be87863f3f4d5b89d52302865565/1.PNG)


##### Register an account

![Registration](https://trello-attachments.s3.amazonaws.com/5f9885f70ed68b256550c3d8/577x433/f47595e970058f13c6b1d63dab2d27b9/register_diagram.jpg)


##### Log out

![Log out](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f988f554b5cc7504f3798f6/d3a7ed0caca4bd3717e7b46c2d338d95/log_out.drawio.png)



### Database

(not available now, will be available in Sprint-3)

### User Interface



## Implementation


#### login and authentication

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/d5c874a48c98cbc81c96596dedd1e6fe/so.PNG)

#### group chat

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/de782d63bc0a247e24fb126d429c9627/sendg.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/0387477c1b226d2e386405a95299f049/ren.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/33d13ad66e10552dc1c3fb927bedc9fc/ngid.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/434dc3032072d09fad50c785bd2eca34/gid.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/2cbd202ddff417d2919034316621b044/data.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/98b61620b247292e8f45f2764aba03a6/const.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/b0662f0670fa385b252bcfc56afb8ab9/conmdb.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/e8ed88bf634b915f6218de84a74e0114/addss.PNG)

![Implementation](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f99915af50b8047c160a0eb/1670c6b4c31aee2abc3b6b7905c3fe7b/add.PNG)

#### edit and check for personal information

![Implementation](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9adc35b4dac58228bd4b68/60f2919a25341809129f103a7e276b34/20201029230042.png)


![Implementation](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9adc35b4dac58228bd4b68/8aefcbe5815192fe56c9bc43060b2c59/20201029225859.png)


![Implementation](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9adc35b4dac58228bd4b68/4da4bf37f15bee06899f191b74e2de8c/20201029225429.png)


![Implementation](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f9adc35b4dac58228bd4b68/e470b2c5df210f5f5f56b9e8b52492ee/20201029225412.png)


The front end will ask the user option to fill in the personal information on the registered screen, and then the information will be uploaded to the back-end and uploaded to the database of mongdb. 
When a person selects his friends, the backend will get the username of the friend from all the online users. When the user chooses to check the friend's information,
the chatServer system will retrieve the information from mongdb and upload it to the clinet server.


### Deployment

We will deploy our app on Heroku

## Software Process Management

(Start from Sprint 0, keep updating)

Introduce how your team uses a software management process, e.g., Scrum, how your teamwork, collaborate.

Include the Trello board with product backlog and sprint cycles in an overview figure and also in detail description. (Main focus of Sprint 0)

Also, include the Gantt chart reflects the timeline from the Trello board. (Main focus of Sprint 0)

### Scrum process


#### Sprint 0

Duration: 09/01/2020-09/10/2020

##### Completed Tasks:

 1.build up our Trello board, our team repo
 
 2.we finish editing the bitbucket about the massenger application
 
 3.each of us added one user requirement and put them in corresponding sprint label in Trello

##### Contributions:

 1.Hanwen Jiang, 3.5 hours, contributed in modifying the Trello board,draw the user diagram, editing the README file.
 
 2.Siqiang Fan, 3.5 hours, contributed in building up team stuffs and setting up the google slides, editing the README file.
 
 3.Mingqing Luo, 3.5 hours, contributed in editing the README file(more)


#### Sprint 1

Duration: 9/10/2020-10/05/2020

##### Completed Tasks:

1. the design of user interface of login, public chat, private chat, add friend system and CSS design

2. front end of typing status of public chat and private chat and maintain the user list

3. back end of the login, send and receive the message (both public and private chat), add friend system.


##### Contributions:

1. Hanwen Jiang, 6h in front end and back end of typing status, partial css design, main documentation

2. Mingqing Luo, 7h contributed coding the add firend system, userlist system, private chat system write the User guide/Deno, write the user cases, build the deploy, editing the README file

3. Siqiang Fan, 5.5h Separation between login and chat UI, Different UIs for public chat and CSS. Simple login with username, logged-in users can send and receive public messages. No temporary and should-be-ignored files/folders. write Use-Case Realization and Implementation. Work on Trello.


#### Sprint 2
Duration: 10/06/2020-10/28/2020


##### Completed Tasks:

1. user interface of registration, login in with username and password, log out, group chat and send message.

2. front end of user registration, advanced login, authentication, group chat, edit and view personal information.

3. back end of user registration, advanced login, authentication, group chat, edit and view personal information

4. data layer of user registration, advanced login, authentication, group chat, edit and view personal information

5. handle some corner cases, such as the invalid input and clear out the data once user send


##### Contributions:

1. Hanwen Jiang, 7h in registration, authentication, user log out, handle corner cases, improve user interface, main documentation

2. Mingqing Luo,7h contributed coding the upload user information system, check friends system, write the User guide/Deno, write the user cases, build the deploy, editing the README file

3. Siqiang Fan, 10 hours, 1.1.5 Logged-in users can create a group chat (more than 2 members). 1.1.6 Logged-in users in a group chat can send/receive messages from the group. 1.1.7 Seperated chat window for group chat. use caes diagram and implementation sequence diagram.

##### Sprint Retrospective:


Good: we meet the basic requirement of the user cases, and the appropriate user interface design

Could have been better: our user interface aesthetics need to be improved, more user cases need to be added, some specific cases we should handle like  add friend, our friend must be permanent, which whenever we login, the friend should be kept in our friend list.

How to improve?

need to start work early, distribute the task to each team member more clear, should make plan before we do the sprint, more experience needed.


## User guide/Demo

![User guide1](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b11415706f73c82276927/9130ad968ad53ad9e248eb12e04e35be/image.png)
When you open the website, please enter the login and password , if you put empty on password it will The default is blank.
![User guide2](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b114e21cf0f5cf75e1a56/2bcbac6c4e4027fd924654c5f0c4e74d/image.png)
 When someone else logs in, you can select the friends you need to add in the "friend name (who you want to add)" multiple box (Note: this will also be the user list of this website)
![User guide3](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b115857b3366777484007/5cf679be63439549585453dc0878883d/image.png)
The added friends will appear in the box after "choose your firend from the list".
![User guide4](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b115dec799b22ae6ed96b/fabeefebaf1d8d08b0b42a4c51ad5329/image.png)
Send the message you want to send to your friend to the "send message to your friend" box
![User guide5](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b17ad8c21ce104d8b85b2/15b4b6e9ab24c781c149277a36c42bf6/image.png)

![User guide6](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f7b17b03e9144788a348b25/e9439f42917470b887e08fd644ab7903/image.png)
This message will only be visible to you and will be displayed in the lower left corner of the private chat box.

(Sprint 2, keep updating)


![User guide2.1](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/be27a24f0432e7ba8dedc5281645d33f/20201028140859.png)
customer can upload the more information when register now.

![User guide2.2](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/c7d41fe9bcbd2bc1ee09e3bc10fedf39/20201028140918.png)
system will save all of the data to the database, after register suceessful.

![User guide2.3](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/4ae62bd891431b855e0f6d140c5d2e46/20201028141022.png)
login now

![User guide2.4](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/0c10e2e80b0d6ea5cc65048714860561/20201028141045.png)


![User guide2.5](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/ad2046aae88b68af3f4e4c8f798d80d0/20201028141059.png)
login in to another account, there can see all account was show on the firends list.

![User guide2.6](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/ad2046aae88b68af3f4e4c8f798d80d0/20201028141059.png)
chosen the firends you want to add.

![User guide2.7](https://trello-attachments.s3.amazonaws.com/5f57f03cb1674501defbb36c/5f990bef8da3dd665a6ff433/3c05b9daeeeb20bab18602e5ee144705/20201028141122.png)
then the information show on the friends information on another account.

![User guide](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f9af78b5489b93872994e6e/2141ac7d0c556f3688b19f2a499f261f/1.PNG)User can use "add group" button

![User guide](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f9af78b5489b93872994e6e/2141ac7d0c556f3688b19f2a499f261f/1.PNG)

![User guide](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f9af78b5489b93872994e6e/da7f6fda35210f99a86e792b3efd93e9/2.PNG) User choose group members

![User guide](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f9af78b5489b93872994e6e/06ad4012d2134756b1959701a9d30596/3.PNG)After created a group, user can found group chat area.

![User guide](https://trello-attachments.s3.amazonaws.com/5f4e94ee015d65248c91e943/5f9af78b5489b93872994e6e/c3482808a4481713619c7f4af3378965/4.PNG)User can send and receive message from group members